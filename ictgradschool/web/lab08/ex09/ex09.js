"use strict";

var car = {
    year : 2007,
    make : "BMW",
    model : "323i",
    'body type' : "Sedan",
    transmition: "Tiptronic",
    odometer: 68512,
    price : "$16000"
};

var music_Album = {
    Title : 1989,
    Artist: "Taylor Swift",
    Year: 2014,
    Genre: "Pop",
    Tracks: [
        "Welcome to New York",
        "Blank Space",
        "Style",
        "Out of the Woods",
        "All You Had to Do Was Stay",
        "Shake it Off",
        "I Wish You Would",
        "Bad Blood",
        "Wildest Dreams",
        "This Love",
        "I Know Places",
        "Clean"
    ],
    ToString : function(){
        return "\"" + this.Title + "\", released in " + this.Year +" by " + this.Artist;
    }
}

console.log(music_Album.ToString());


debugger;

console.log("Finished");
console.log(car["body type"]);


